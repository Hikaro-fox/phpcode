<? include("include/data.php"); ?>
<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<title>Главная страница</title>
	</head>
	<body>
	<? include("include/header.php"); ?>
	<div class="content">
		<div class="articles-list">
			<? foreach ($articlesCollection as $articles): ?>
				<div class="articles-item">
					<a href="/articles/?id=<?=$articles["id"]?>"><?=$articles["title"]?></a><br>
					<img src="<?=$articles["pathImage"]?>" width="300" <br>
					<p><?=$articles["previewText"]?></p>
				</div>
			<? endforeach; ?>
		</div>
		<div class="authors-list">
			<? foreach ($authorsCollection as $authors):  ?>
				<div class="authors-item">
					<a href="/authors/?id=<?=$authors["id"]?>"><?=$authors["surname"]?> <?=$authors["name"]?> <?=$authors["patronymic"]?></a><br>
					<img src="<?=$authors["pathImage"]?>" width="300" <br>
					<p><?=$authors["dob"]?></p>
				</div>
			<? endforeach; ?>
		</div>
	</div>
	<? include("include/footer.php"); ?>
	</body>
</html>
