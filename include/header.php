<?
	$menuCollection = [
		[
			"title" => "Главная страница",
			"url" => "/"
		],
		[
			"title" => "Страница статей",
			"url" => "/articles/"
		],
		[
			"title" => "Страница авторов",
			"url" => "/authors/"
		]
	];
?>
<div class="header">
	<h1>Главная страница</h1>
	<img src="/images/logo.jpeg" alt="лого" width="300">
	<ul>
		<? foreach ($menuCollection as $menu): ?>
			<li><a href="<?= $menu["url"] ?>"><?= $menu["title"] ?></a></li>
		<? endforeach; ?>
	</ul>
</div>
