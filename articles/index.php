<?
include ('../include/data.php');
if (!empty($_GET['id']) && $_GET['id'] !== '') {
    $key = array_search($_GET['id'], array_column($articlesCollection, 'id'));
    $title = $articlesCollection[$key]['title'];
}
else {
    $title = 'Страница статей';
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
</head>
<body>
<? include("../include/header.php"); ?>
<? if (!empty($_GET['id']) && $_GET['id'] !== ''): ?>
<? else: ?>
    <div class="articles-list">
        <? foreach ($articlesCollection as $articles): ?>
            <div class="articles-item">
                <a href="/articles/?id=<?=$articles["id"]?> "><?=$articles["title"]?></a><br>
                <img src="<?=$articles["pathImage"]?>" width="300" <br>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>
<? include("../include/footer.php"); ?>
</body>
</html>
