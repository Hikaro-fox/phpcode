<?
include ('../include/data.php');
if (!empty($_GET['id']) && $_GET['id'] !== '') {
$key = array_search($_GET['id'], array_column($authorsCollection, 'id'));
$title = $authorsCollection[$key]['title'];
}
else {
$title = 'Страница авторов';
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
</head>
<body>
<? include("../include/header.php"); ?>
<? if (!empty($_GET['id']) && $_GET['id'] !== ''): ?>
<? else: ?>
    <div class="articles-list">
        <? foreach ($authorsCollection as $authors): ?>
            <div class="articles-item">
                <a href="/authors/?id=<?=$authors["id"]?> "><?=$authors["title"]?></a><br>
                <img src="<?=$authors["pathImage"]?>" width="300" <br>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>
<? include("../include/footer.php"); ?>
</body>
